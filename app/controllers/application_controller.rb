class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user
  
  def current_user
    @current_user ||= User.find_by(token: session[:current_user_id])
  end

  private

  def authorize
    redirect_to log_in_path, alert: "Not authorized" if current_user.nil?
  end
  
end
