class SessionsController < ApplicationController

  def create
    @user = User.find_by(email: params[:email])
    if @user && @user.authenticate(params[:password])
      session[:current_user_id] = @user.token
      flash[:success] = 'Welcome'
      redirect_to root_url
    else
      flash.now[:error] = 'Invalid email or password'
      render :new
    end
  end

  def destroy
    session[:current_user_id] = nil
    flash[:success] = 'Good bye'
    redirect_to root_url
  end
end
